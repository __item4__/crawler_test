#!/usr/bin/env python
# -*- coding:utf-8 -*-

import aiohttp
import asyncio
import json
import os
import os.path
import re
import sys


USER_AGENT = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.17 ' + \
             '(KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17'
JSON_URL = 'http://instagram.com/{username}/media?max_id={max_id}'


@asyncio.coroutine
def load_page(instagram_id):
    response = yield from aiohttp.request(
        'GET',
        'http://instagram.com/{username}'.format(username=instagram_id),
        headers={'User-Agent': USER_AGENT}
    )
    body = yield from response.read_and_close()

    match = re.search(
        r'<script\s*type="text/javascript">'
        r'window._sharedData\s*=\s*(.+?);\s*</script>',
        body.decode('u8'),
        re.S
    )

    if match:
        data = json.loads(match.group(1))
        image_id = None
        for user in data['entry_data']['UserProfile']:
            for line in user['userMedia']:
                image_id = line['id']
                referer = line['link'] + '?modal=true'
                width = 0
                selected_img = None
                for img in line['images'].keys():
                    temp = int(line['images'][img]['width'])
                    if width < temp:
                        selected_img = line['images'][img]
                        width = temp
                yield from asyncio.async(
                    save_file(selected_img['url'], referer)
                )
        yield from asyncio.async(load_json(instagram_id, image_id))
    else:
        print('존재하지 않거나 파싱할 수 없는 페이지', file=sys.stderr)


@asyncio.coroutine
def load_json(instagram_id, max_id):
    response = yield from aiohttp.request(
        'GET',
        JSON_URL.format(username=instagram_id, max_id=str(max_id)),
        headers={'User-Agent': USER_AGENT}
    )
    body = yield from response.read_and_close()
    data = json.loads(body.decode('u8'))
    image_id = None
    if data['status'] == 'ok':
        for line in data['items']:
            image_id = line['id']
            referer = line['link'] + '?modal=true'
            width = 0
            selected_img = None
            for img in line['images'].keys():
                temp = int(line['images'][img]['width'])
                if width < temp:
                    selected_img = line['images'][img]
                    width = temp
            yield from asyncio.async(save_file(selected_img['url'], referer))
    if data['more_available']:
        yield from asyncio.async(load_json(instagram_id, image_id))


@asyncio.coroutine
def save_file(url, referer):
    filename = url.split('/')[-1]
    response = yield from aiohttp.request(
        'GET',
        url,
        headers={'User-Agent': USER_AGENT, 'Referer': referer}
    )
    data = yield from response.read_and_close()
    if not os.path.isdir('data'):
        os.mkdir('data')
    with open(os.path.join('data', filename), 'wb') as f:
        f.write(data)


def main():
    if len(sys.argv) < 2:
        print('아이디를 지정해주세요', file=sys.stderr)
        raise SystemExit(1)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(load_page(sys.argv[1]))
    loop.close()

if __name__ == '__main__':
    main()
